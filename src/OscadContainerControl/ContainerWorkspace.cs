using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace OscadContainerControl;

public class ContainerWorkspace
{
    public const string WorkspaceDir = "/workspace";
    private const string TmpPreviewFile = "/tmp/preview.png";
    private const string TmpModelFile = "/tmp/model.stl";
    private const string LocalWorkspacesDir = "workspaces";

    public ContainerWorkspace(string id)
    {
        Id = id;
        Container = new DockerContainer();
    }

    public string Id { get; init; }

    public DockerContainer? Container { get; set; }
    public int PreviewWidth { get; set; } = 256;
    public int PreviewHeight { get; set; } = 256;

    public string LocalWorkspaceDir => LocalWorkspacesDir + "/" + Id;
    public string LocalPreviewFile => LocalWorkspacesDir + "/" + Id + "/preview.png";
    public string LocalModelFile => LocalWorkspacesDir + "/" + Id + "/model.stl";
    public string LocalLogFile => LocalWorkspacesDir + "/" + Id + "/output.log";

    public void Start()
    {
        CreateLocalDir();
        Container?.Start(Id);
    }

    private void CreateLocalDir()
    {
        if (!Directory.Exists(LocalWorkspacesDir))
        {
            Directory.CreateDirectory(LocalWorkspacesDir);
        }

        Directory.CreateDirectory(LocalWorkspaceDir);
    }

    public IEnumerable<FileDescription> ListFiles(string? directoryName = "")
    {
        var secureDir = DockerContainer.SecurePath(string.IsNullOrWhiteSpace(directoryName)?".":directoryName);
        var output = Container?.Exec($"tree -a -J \"{secureDir}\"");
        if (string.IsNullOrWhiteSpace(output))
        {
            return Enumerable.Empty<FileDescription>();
        }

        var fileTrees = JsonSerializer.Deserialize<IEnumerable<FileTree>>(output);
        if (fileTrees == null)
        {
            return Enumerable.Empty<FileDescription>();
        }

        return fileTrees
            .Where(f => f.type != "report")
            .Select(f => new FileDescription
            {
                type = f.type,
                name = f.name,
                target = f.target,
                contents = f.contents
            });
    }

    public void AddFile(string file, string destination)
    {
        if (!File.Exists(file))
        {
            throw new FileNotFoundException("file does not exist", file);
        }

        Container?.CopyToContainer(file, $"{WorkspaceDir}/{destination}");
    }

    public string GetFile(string filename)
    {
        var localFileName = $"{LocalWorkspaceDir}/temp.file";
        Container?.CopyFromContainer($"{WorkspaceDir}/{filename}", localFileName);
        return localFileName;
    }

    public void CreateDirectory(string directoryPath)
    {
        Container?.Exec($"mkdir -p \"{WorkspaceDir}/{DockerContainer.SecurePath(directoryPath)}\"");
    }

    public void DeleteFile(string fileName)
    {
        Container?.Exec($"rm -f \"{WorkspaceDir}/{DockerContainer.SecurePath(fileName)}\"");
    }

    public void DeleteDirectory(string directoryPath)
    {
        Container?.Exec($"rm -Rf \"{WorkspaceDir}/{DockerContainer.SecurePath(directoryPath)}\"");
    }

    public byte[] Preview(IDictionary<string, string> variables, string file)
    {
        var vars = BuildVariables(variables);
        var output =
            Container?.TryExec(
                $"xvfb-run -a openscad -o {TmpPreviewFile} {vars} --imgsize {PreviewWidth},{PreviewHeight} \"{DockerContainer.SecurePath(file)}\"");
        Container?.CopyFromContainer(TmpPreviewFile, LocalPreviewFile);
        File.WriteAllText(LocalLogFile, output?.StdOut + "\n" + output?.StdErr);
        return File.ReadAllBytes(LocalPreviewFile);
    }

    public byte[] Render(IDictionary<string, string> variables, string file)
    {
        var vars = BuildVariables(variables);
        var output = Container?.TryExec($"xvfb-run -a openscad -o {TmpModelFile} {vars} \"{DockerContainer.SecurePath(file)}\" --render");
        Container?.CopyFromContainer(TmpModelFile, LocalModelFile);
        File.WriteAllText(LocalLogFile, output?.StdOut + "\n" + output?.StdErr);
        return File.ReadAllBytes(LocalModelFile);
    }

    internal static string BuildVariables(IDictionary<string, string> variables)
    {
        var cmdVars = new StringBuilder();
        foreach (var (key, value) in variables)
        {
            CheckVarName(key);
            var securedValue = DockerContainer.Secure(value);
            if (!IsNumberOrBoolean(securedValue))
            {
                securedValue = $"\\\"{securedValue}\\\"";
            }
            cmdVars.Append($" -D \"{key}={securedValue}\" ");
        }
        return cmdVars.ToString();
    }

    private static bool IsNumberOrBoolean(string value)
    {
        return value is "true" or "false" || Regex.IsMatch(value, @"^[+-]?(\d+\.?|\d*\.\d+)([Ee][+-]?\d+)?$");
    }

    private static void CheckVarName(string value)
    {
        if (!Regex.IsMatch(value, "^[$a-zA-Z0-9_][a-zA-Z0-9_]*$"))
        {
            throw new ContainerException($"Variable name is invalid, {value} does not match regexp ^[a-ZA-Z0-9_]*$");
        }
    }
}