namespace OscadContainerControl;

public class FileTree
{
    public string? type { get; set; }
    public string? name { get; set; }
    public string? target { get; set; }
    public IEnumerable<FileDescription>? contents { get; set; }
    public int? directories { get; set; }
    public int? files { get; set; }
}