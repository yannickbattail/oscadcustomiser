using System.Diagnostics;
using System.Text;

namespace OscadContainerControl;

internal static class Processus
{
    internal static string RunSuccess(string filename, string? arguments = null)
    {
        var output = Run(filename, arguments);
        if (output.ExitCode != 0)
        {
            throw new ContainerException(
                $"command {filename} {arguments} failed. exitCode={output.ExitCode}. StdErr={output.StdErr}");
        }

        return output.StdOut + output.StdErr;
    }

    internal static ProcessOut Run(string filename, string? arguments = null)
    {
        var process = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = filename,
                Arguments = arguments,
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            }
        };
        var stdOut = new StringBuilder();
        var stdErr = new StringBuilder();

        process.OutputDataReceived += (sender, args) => stdOut.AppendLine(args.Data);
        process.ErrorDataReceived += (sender, args) => stdErr.AppendLine(args.Data);

        try
        {
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
        }
        catch (Exception e)
        {
            throw new ContainerException($"OS error while executing {filename} {arguments} ERROR: {e.Message}", e);
        }

        return new ProcessOut
        {
            ExitCode = process.ExitCode,
            StdOut = stdOut.ToString(),
            StdErr = stdErr.ToString()
        };
    }
}