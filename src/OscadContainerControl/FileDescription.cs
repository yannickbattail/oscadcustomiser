namespace OscadContainerControl;

public class FileDescription
{
    public string? type { get; set; }
    public string? name { get; set; }
    public string? target { get; set; }
    public IEnumerable<FileDescription>? contents { get; set; }
}