namespace OscadContainerControl;

public class DockerContainer
{
    private const string Image = "oscadcustomiser";
    private const string Tag = "workspace";
    private int _sleepTime;
    public string? Id { get; set; }
    public DateTime? LastStart { get; set; }

    public virtual void Start(string name, int sleepTime = 600)
    {
        _sleepTime = sleepTime;
        Id = Processus.RunSuccess("docker", $"container run -d --name \"{Secure(name)}\" {Image}:{Tag} sleep {sleepTime}").Trim();
        LastStart = DateTime.Now;
    }

    private void CheckStart()
    {
        if (LastStart == null || LastStart?.AddSeconds(_sleepTime) < DateTime.Now)
        {
            ReStart();
        }
    }

    private void ReStart()
    {
        Processus.Run("docker", $"start {Id}");
        LastStart = new DateTime();
    }

    public virtual string Exec(string command)
    {
        CheckStart();
        return Processus.RunSuccess("docker", $"container exec {Id} {command}");
    }

    public virtual ProcessOut TryExec(string command)
    {
        CheckStart();
        return Processus.Run("docker", $"container exec {Id} {command}");
    }

    public virtual void CopyFromContainer(string sourceFileInContainer, string destinationLocalFile)
    {
        CheckStart();
        Processus.RunSuccess("docker", $"cp \"{Id}:{SecurePath(sourceFileInContainer)}\" \"{SecurePath(destinationLocalFile)}\"");
    }

    public virtual void CopyToContainer(string sourceLocalFile, string destinationFileInContainer)
    {
        CheckStart();
        Processus.RunSuccess("docker", $"cp \"{SecurePath(sourceLocalFile)}\"  \"{Id}:{SecurePath(destinationFileInContainer)}\"");
    }
    public static string Secure(string value)
    {
        return value.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }
    public static string SecurePath(string value)
    {
        return Secure(value).Replace("..", ".").Replace("..", ".").Replace("..", ".");
    }
}