namespace OscadContainerControl;

public class ProcessOut
{
    public string StdOut { get; set; } = "";
    public string StdErr { get; set; } = "";
    public int ExitCode { get; set; }
}