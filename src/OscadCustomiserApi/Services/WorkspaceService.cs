using OscadContainerControl;
using OscadCustomisationParser;
using OscadCustomisationParser.Model;
using OscadCustomiserApi.Models;
using OscadCustomiserApi.Repositories;

namespace OscadCustomiserApi.Services;

public class WorkspaceService : IWorkspaceService
{
    private readonly IWorkspaceRepository _repository;

    public WorkspaceService(IWorkspaceRepository repository)
    {
        _repository = repository;
    }

    public Workspace CreateWorkspace()
    {
        var workspace = new Workspace(Guid.NewGuid().ToString());
        workspace.Start();
        workspace.Name = "Workspace #" + (_repository.GetWorkspaces().Count() + 1);
        _repository.SaveWorkspace(workspace);
        return workspace;
    }

    public byte[]? Preview(string workspaceId, IDictionary<string, string> values, string file)
    {
        try
        {
            return _repository.GetWorkspace(workspaceId).Preview(values, file);
        }
        catch (Exception)
        {
            return null;
        }
    }

    public byte[]? Render(string workspaceId, IDictionary<string, string> values, string file)
    {
        try
        {
            return _repository.GetWorkspace(workspaceId).Render(values, file);
        }
        catch (Exception)
        {
            return null;
        }
    }

    public string? GetLastLog(string workspaceId)
    {
        try
        {
            return File.ReadAllText(_repository.GetWorkspace(workspaceId).LocalLogFile);
        }
        catch (Exception)
        {
            return null;
        }
    }

    public Workspace GetWorkspace(string workspaceId)
    {
        return _repository.GetWorkspace(workspaceId);
    }

    public IEnumerable<Workspace> GetWorkspaces()
    {
        return _repository.GetWorkspaces();
    }

    public Workspace SaveWorkspace(Workspace workspace)
    {
        try
        {
            var fromDb = _repository.GetWorkspace(workspace.Id);
            fromDb.Name = workspace.Name;
            fromDb.PreviewWidth = workspace.PreviewWidth;
            fromDb.PreviewHeight = workspace.PreviewHeight;
            return _repository.SaveWorkspace(fromDb);
        }
        catch (WorkspaceRepositoryException)
        {
            return _repository.SaveWorkspace(workspace);
        }
    }

    public void DeleteWorkspaces(string workspaceId)
    {
        _repository.DeleteWorkspaces(workspaceId);
    }

    public IEnumerable<Customization> GetCustomiserValues(string workspaceId, string filePath)
    {
        try
        {
            var workspaceFilePath = _repository.GetWorkspace(workspaceId).GetFile(filePath);
            var fileContent = File.ReadAllText(workspaceFilePath);
            return new OpenscadParser().Parse(fileContent);
        }
        catch (Exception)
        {
            return Enumerable.Empty<Customization>();
        }
    }

    public void AddFile(string workspaceId, Stream fileStream, string destinationPath)
    {
        var workspace = _repository.GetWorkspace(workspaceId);
        const string fileName = "temp.file";
        using (var workspaceFileStream =
               new FileStream($"{workspace.LocalWorkspaceDir}/{fileName}", FileMode.Create, FileAccess.Write))
        {
            fileStream.CopyTo(workspaceFileStream);
        }

        workspace.AddFile($"{workspace.LocalWorkspaceDir}/{fileName}", destinationPath);
    }

    public byte[] GetFile(string workspaceId, string fileName)
    {
        return File.ReadAllBytes(_repository.GetWorkspace(workspaceId).GetFile(fileName));
    }

    public void DeleteFile(string workspaceId, string fileName)
    {
        _repository.GetWorkspace(workspaceId).DeleteFile(fileName);
    }

    public IEnumerable<FileDescription>? ListFiles(string workspaceId, string? directoryName = "")
    {
        return _repository.GetWorkspace(workspaceId).ListFiles(directoryName);
    }

    public void CreateDirectory(string workspaceId, string directoryPath)
    {
        _repository.GetWorkspace(workspaceId).CreateDirectory(directoryPath);
    }

    public void DeleteDirectory(string workspaceId, string directoryPath)
    {
        _repository.GetWorkspace(workspaceId).DeleteDirectory(directoryPath);
    }
}