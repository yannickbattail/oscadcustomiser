using OscadContainerControl;
using OscadCustomisationParser.Model;
using OscadCustomiserApi.Models;

namespace OscadCustomiserApi.Services;

public interface IWorkspaceService
{
    Workspace CreateWorkspace();
    byte[]? Preview(string workspaceId, IDictionary<string, string> values, string file);
    byte[]? Render(string workspaceId, IDictionary<string, string> values, string file);
    string? GetLastLog(string workspaceId);
    Workspace? GetWorkspace(string workspaceId);
    IEnumerable<Workspace> GetWorkspaces();
    Workspace SaveWorkspace(Workspace workspace);
    void DeleteWorkspaces(string workspaceId);
    IEnumerable<Customization> GetCustomiserValues(string workspaceId, string filePath);
    void AddFile(string workspaceId, Stream fileStream, string destinationPath);
    byte[] GetFile(string workspaceId, string fileName);
    IEnumerable<FileDescription>? ListFiles(string workspaceId, string? directoryName = "");
    void CreateDirectory(string workspaceId, string directoryPath);
    void DeleteFile(string workspaceId, string fileName);
    void DeleteDirectory(string workspaceId, string directoryPath);
}