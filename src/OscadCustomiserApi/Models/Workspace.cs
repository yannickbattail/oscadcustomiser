using OscadContainerControl;

namespace OscadCustomiserApi.Models;

public class Workspace : ContainerWorkspace
{
    public Workspace(string id) : base(id)
    {
    }

    public string Name { get; set; } = "";
}