using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OscadCustomiserApi.Models;
using OscadCustomiserApi.resources;
using OscadCustomiserApi.Services;

namespace OscadCustomiserApi.Controllers;

//[Authorize]
[ApiController]
[Route("[controller]")]
//[RequiredScope(RequiredScopesConfigurationKey = "AzureAd:Scopes")]
public class WorkspaceController : ControllerBase
{
    private readonly IWorkspaceService _workspaceService;

    public WorkspaceController(IWorkspaceService workspaceService)
    {
        _workspaceService = workspaceService;
    }

    [HttpGet]
    [Route("")]
    public IEnumerable<WorkspaceResource> GetAll()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AllowNullCollections = true;
            cfg.CreateMap<Workspace, WorkspaceResource>();
        });
        var mapperOut = new Mapper(configuration);
        return mapperOut.Map<IEnumerable<WorkspaceResource>>(_workspaceService.GetWorkspaces());
    }

    [HttpGet]
    [Route("create")]
    public WorkspaceResource Create()
    {
        var mapperOut = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<Workspace, WorkspaceResource>()));
        return mapperOut.Map<WorkspaceResource>(_workspaceService.CreateWorkspace());
    }

    [HttpGet]
    [Route("{workspaceId}")]
    public WorkspaceResource GetWorkspace(string workspaceId)
    {
        var mapperOut = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<Workspace, WorkspaceResource>()));
        return mapperOut.Map<WorkspaceResource>(_workspaceService.GetWorkspace(workspaceId));
    }

    [HttpPut]
    [Route("")]
    public WorkspaceResource SaveWorkspace(WorkspaceResource workspace)
    {
        var mapperIn = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<WorkspaceResource, Workspace>()));
        var mapperOut = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<Workspace, WorkspaceResource>()));
        return mapperOut.Map<WorkspaceResource>(_workspaceService.SaveWorkspace(mapperIn.Map<Workspace>(workspace)));
    }

    [HttpDelete]
    [Route("{workspaceId}")]
    public void DeleteWorkspaces(string workspaceId)
    {
        _workspaceService.DeleteWorkspaces(workspaceId);
    }

    [HttpPost]
    [Route("{workspaceId}/preview")]
    public FileContentResult Preview(string workspaceId, RenderingValues renderingValues)
    {
        var values = new Dictionary<string, string>();
        foreach (var customisationValue in renderingValues.CustomisationValues)
        {
            if (customisationValue.Variable != null)
            {
                values[customisationValue.Variable] = customisationValue.Value ?? "";
            }
        }

        var png = _workspaceService.Preview(workspaceId, values, renderingValues.FilePath);
        return File(png, "image/png");
    }

    [HttpPost]
    [Route("{workspaceId}/render")]
    public FileContentResult Render(string workspaceId, RenderingValues renderingValues)
    {
        var values = new Dictionary<string, string>();
        foreach (var customisationValue in renderingValues.CustomisationValues)
        {
            if (customisationValue.Variable != null)
            {
                values[customisationValue.Variable] = customisationValue.Value ?? "";
            }
        }

        var stl = _workspaceService.Render(workspaceId, values, renderingValues.FilePath);
        return File(stl, "model/stl");
    }

    [HttpGet]
    [Route("{workspaceId}/lastLog")]
    public string? GetLastLog(string workspaceId)
    {
        return _workspaceService.GetLastLog(workspaceId);
    }
}