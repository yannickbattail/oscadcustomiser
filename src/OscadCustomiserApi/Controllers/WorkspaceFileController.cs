using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using OscadContainerControl;
using OscadCustomisationParser.Model;
using OscadCustomiserApi.resources;
using OscadCustomiserApi.Services;

namespace OscadCustomiserApi.Controllers;

//[Authorize]
[ApiController]
[Route("[controller]/{workspaceId}")]
//[RequiredScope(RequiredScopesConfigurationKey = "AzureAd:Scopes")]
public class WorkspaceFileController : ControllerBase
{
    private readonly ILogger<WorkspaceController> _logger;

    private readonly IWorkspaceService _workspaceService;

    public WorkspaceFileController(ILogger<WorkspaceController> logger, IWorkspaceService workspaceService)
    {
        _logger = logger;
        _workspaceService = workspaceService;
    }

    [HttpGet]
    [Route("directory/{directoryName?}")]
    public IEnumerable<FileDescription>? ListDirectory(string workspaceId, string? directoryName = "")
    {
        return _workspaceService.ListFiles(workspaceId,
            directoryName != null ? Uri.UnescapeDataString(directoryName) : null);
    }

    [HttpPut]
    [Route("directory/{directoryName}")]
    public void CreateDirectory(string workspaceId, string directoryName)
    {
        _workspaceService.CreateDirectory(workspaceId, Uri.UnescapeDataString(directoryName));
    }

    [HttpDelete]
    [Route("directory/{directoryName}")]
    public void DeleteDirectory(string workspaceId, string directoryName)
    {
        _workspaceService.DeleteDirectory(workspaceId, Uri.UnescapeDataString(directoryName));
    }

    [HttpGet]
    [Route("file/{filePath}")]
    public FileContentResult GetFile(string workspaceId, string filePath = "")
    {
        var fileContent = _workspaceService.GetFile(workspaceId, Uri.UnescapeDataString(filePath));
        new FileExtensionContentTypeProvider().TryGetContentType(filePath, out var contentType);
        return File(fileContent, contentType ?? "application/octet-stream", filePath);
    }

    [HttpPost]
    [Route("file/")]
    public string AddFile(string workspaceId, FileContent file)
    {
        var fileStream = new MemoryStream(Encoding.UTF8.GetBytes(file.Content));
        _workspaceService.AddFile(workspaceId, fileStream, file.FileName);
        return file.FileName;
    }


    [HttpPut]
    [Route("file/")]
    public string UploadFile(string workspaceId)
    {
        var uploadedFile = HttpContext.Request.Form.Files.Count > 0 ? HttpContext.Request.Form.Files[0] : null;
        if (uploadedFile == null)
        {
            throw new FileNotFoundException();
        }

        var memoryStream = new MemoryStream();
        uploadedFile.CopyToAsync(memoryStream);
        memoryStream.Position = 0;
        _workspaceService.AddFile(workspaceId, memoryStream, uploadedFile.FileName);
        return uploadedFile.FileName;
    }

    [HttpDelete]
    [Route("file/{filePath}")]
    public void DeleteFile(string workspaceId, string filePath)
    {
        _workspaceService.DeleteFile(workspaceId, Uri.UnescapeDataString(filePath));
    }

    [HttpGet]
    [Route("customisation/{filePath}")]
    public IEnumerable<Customization> GetCustomiserValues(string workspaceId, string filePath)
    {
        return _workspaceService.GetCustomiserValues(workspaceId, Uri.UnescapeDataString(filePath));
    }
}