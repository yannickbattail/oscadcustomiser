using System.ComponentModel.DataAnnotations;

namespace OscadCustomiserApi.resources;

public class RenderingValues
{
    [Required] public string? FilePath { get; set; }

    [Required] public IEnumerable<VarValue> CustomisationValues { get; set; } = new List<VarValue>();
}