namespace OscadCustomiserApi.resources;

public class WorkspaceResource
{
    public string Id { get; init; } = "";
    public string Name { get; set; } = "";
    public int PreviewWidth { get; set; } = 256;
    public int PreviewHeight { get; set; } = 256;
}