namespace OscadCustomiserApi.resources;

public class FileContent
{
    public string FileName { get; set; } = "";
    public string Content { get; set; } = "";
}