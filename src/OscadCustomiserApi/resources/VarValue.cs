namespace OscadCustomiserApi.resources;

public class VarValue
{
    public string? Variable { get; set; }
    public string? Value { get; set; }
}