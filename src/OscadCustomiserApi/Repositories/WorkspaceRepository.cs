using System.Text.Json;
using OscadContainerControl;
using OscadCustomiserApi.Models;

namespace OscadCustomiserApi.Repositories;

public class WorkspaceRepository : IWorkspaceRepository
{
    private readonly string _fileName = "workspaces/WorkspaceRepository.json";

    public WorkspaceRepository(string? saveFile = null)
    {
        if (!string.IsNullOrWhiteSpace(saveFile))
        {
            _fileName = saveFile;
        }

        if (!File.Exists(_fileName))
        {
            Console.WriteLine("No previous saved workspaces");
            return;
        }

        try
        {
            Workspaces = JsonSerializer.Deserialize<Dictionary<string, Workspace>>(File.ReadAllText(_fileName)) ??
                         new Dictionary<string, Workspace>();
            Console.WriteLine("Workspaces loaded from " + _fileName);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    private IDictionary<string, Workspace> Workspaces { get; } = new Dictionary<string, Workspace>();

    public Workspace GetWorkspace(string workspaceId)
    {
        if (Workspaces.ContainsKey(workspaceId))
        {
            return Workspaces[workspaceId];
        }

        throw new WorkspaceRepositoryException("no such workspace " + workspaceId);
    }

    public IEnumerable<Workspace> GetWorkspaces()
    {
        return Workspaces.Values;
    }

    public Workspace SaveWorkspace(Workspace workspace)
    {
        Workspaces[workspace.Id] = workspace;
        File.WriteAllText(_fileName, JsonSerializer.Serialize(Workspaces));
        return workspace;
    }

    public void DeleteWorkspaces(string workspaceId)
    {
        Workspaces.Remove(workspaceId);
        File.WriteAllText(_fileName, JsonSerializer.Serialize(Workspaces));
    }
}