using OscadCustomiserApi.Models;

namespace OscadCustomiserApi.Repositories;

public interface IWorkspaceRepository
{
    public Workspace GetWorkspace(string workspaceId);
    public IEnumerable<Workspace> GetWorkspaces();
    public Workspace SaveWorkspace(Workspace workspace);
    public void DeleteWorkspaces(string workspaceId);
}