namespace OscadContainerControl;

[Serializable]
public class WorkspaceRepositoryException : Exception
{
    public WorkspaceRepositoryException()
    {
    }

    public WorkspaceRepositoryException(string? message) : base(message)
    {
    }

    public WorkspaceRepositoryException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}