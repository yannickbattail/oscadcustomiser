WorkspaceController = {
    getAll: function () {
        return fetch('/Workspace')
            .then(response => response.json());
    },
    create: function () {
        return fetch('/Workspace/create')
            .then(response => response.json());
    },
    get: function (workspaceId) {
        return fetch(`/Workspace/${workspaceId}`)
            .then(response => response.json())
    },
    delete: function (workspaceId) {
        return fetch(`/Workspace/${workspaceId}`, {
            method: 'DELETE'
        });
    },
    save: function (workspace) {
        return fetch('/Workspace', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(workspace),
        });
    },
    preview: function (workspaceId, file, customisationVals) {
        return fetch(`/Workspace/${workspaceId}/preview`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({FilePath: file, CustomisationValues: customisationVals}),
        });
    },
    render: function (workspaceId, file, customisationVals) {
        return fetch(`/Workspace/${workspaceId}/render`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({FilePath: file, CustomisationValues: customisationVals}),
        }).then(response => response.blob());
    },
    lastLog: function (workspaceId) {
        return fetch(`/Workspace/${workspaceId}/lastLog`);
    }
};
