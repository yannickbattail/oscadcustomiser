function loading() {
    document.getElementById('loading').style.display = 'inline';
}

function stopLoading() {
    document.getElementById('loading').style.display = 'none';
}

function init(workspace) {
    const url = new URL(window.location.href);
    const workspaceId = url.searchParams.get("workspaceId");
    if (workspaceId) {
        workspace.id = workspaceId;
        console.log('fetchWorkspace ' + workspaceId);
        fetchWorkspace(workspace);
    } else {
        console.log('listWorkspaces');
        listWorkspaces();
    }
}

function listWorkspaces() {
    loading();
    WorkspaceController.getAll()
        .then(data => {
            displayListWorkspace(data);
            stopLoading();
        });
}

function createWorkspace(workspace) {
    loading();
    WorkspaceController.create()
        .then(data => {
            console.log(data);
            workspace.id = data.id;
            workspace.name = data.name;
            workspace.previewWidth = data.previewWidth;
            workspace.previewHeight = data.previewHeight;
            window.location.href = '?workspaceId=' + workspace.id;
        });
}


function saveFileWorkspace(workspace) {
    loading();
    const fileName = document.getElementById('fileName').value;
    const content = document.getElementById('fileContent').value;
    WorkspaceFileController.addFile(workspace.id, fileName, content)
        .then(data => {
            fetchFilesList(workspace);
            getValuesWorkspace(workspace.id, fileName);
            previewWorkspace(workspace, fileName);
            stopLoading();
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function uploadFile(workspace) {
    const input = document.querySelector('input[type="file"]');
    if (!input.files.length) {
        alert('no file to upload');
        return;
    }
    const data = new FormData();
    for (const file of input.files) {
        data.append('files', file, file.name)
    }
    WorkspaceFileController.uploadFile(workspace.id, data)
        .then(values => {
            fetchFilesList(workspace);
            stopLoading();
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });

}

function fetchWorkspace(workspace) {
    loading();
    WorkspaceController.get(workspace.id)
        .then(data => {
            console.log('Success:', data);
            workspace.id = data.id;
            workspace.name = data.name;
            workspace.previewWidth = data.previewWidth;
            workspace.previewHeight = data.previewHeight;
            displayWorkspace(workspace);
            stopLoading();
            fetchFilesList(workspace);
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function saveWorkspace(workspace) {
    loading();
    loadWorkspace(workspace);
    document.getElementById('previewDiv').style.width = workspace.previewWidth + 'px';
    document.getElementById('previewDiv').style.height = workspace.previewHeight + 'px';
    WorkspaceController.save(workspace)
        .then(data => {
            stopLoading();
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function loadFile(workspace, filePath) {
    document.getElementById('fileName').value = filePath;
    document.getElementById('customisation').innerHTML = "";
    document.getElementById('actionBtn').innerHTML = "";
    document.getElementById('previewImage').src = "img/preview.png";
    document.getElementById('fileContent').disabled = true;
    document.getElementById('fileContent').readonly = true;
    document.getElementById('fileContent').value = "-- nothing to show --";
    if (IsOpenScadFile(filePath)) {
        fetchFileContent(workspace, filePath);
        getValuesWorkspace(workspace.id, filePath);
        previewWorkspace(workspace, filePath);
    } else if (IsVectorImageFile(filePath)) {
        fetchFileContent(workspace, filePath);
        preview3dFileWorkspace(workspace, filePath);
    } else if (IsImageFile(filePath)) {
        if (Is2dFile(filePath)) {
            preview2dFileWorkspace(workspace, filePath);
        } else {
            fetchImageContent(workspace, filePath);
        }
    } else if (Is3dFile(filePath)) {
        preview3dFileWorkspace(workspace, filePath);
    } else if (IsVector3dFile(filePath)) {
        preview3dFileWorkspace(workspace, filePath);
    } else if (IsTextFile(filePath)) {
        fetchFileContent(workspace, filePath);
        if (Is2dFile(filePath)) {
            preview2dFileWorkspace(workspace, filePath);
        }
    } else {
        
    }
}

function getLastLogWorkspace(workspace) {
    loading();
    WorkspaceController.lastLog(workspace.id)
        .then(data => {
            data.text()
                .then(value => {
                    document.getElementById('lastLog').innerText = value;
                    stopLoading();
                });
        });
}

function previewWorkspace(workspace, file) {
    loading();
    let customisationVals = loadCustomisation();
    document.getElementById('previewDiv').style.width = workspace.previewWidth + 'px';
    document.getElementById('previewDiv').style.height = workspace.previewHeight + 'px';
    let p = WorkspaceController.preview(workspace.id, file, customisationVals);
    p.then(response => response.blob())
        .then(blob => {
            document.getElementById('previewImage').src = URL.createObjectURL(blob);
            getLastLogWorkspace(workspace);
            stopLoading();
        })
        .catch(data => {
            console.error(data);
            document.getElementById('previewImage').src = "img/preview_failed.png";
            getLastLogWorkspace(workspace);
            stopLoading();
        });
    p.catch(data => {
        console.error(data);
        document.getElementById('previewImage').src = "img/preview_failed.png";
        getLastLogWorkspace(workspace);
        stopLoading();
    });
}

function preview3dFileWorkspace(workspace, file) {
    loading();
    let tmpFile = "./.tmp.scad";
    WorkspaceFileController.addFile(workspace.id, tmpFile, 'import("'+file+'");')
        .then(data => {
            previewWorkspace(workspace, tmpFile)
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function preview2dFileWorkspace(workspace, file) {
    loading();
    let tmpFile = "./.tmp.scad";
    WorkspaceFileController.addFile(workspace.id, tmpFile, 'surface(file = "'+file+'");')
        .then(data => {
            previewWorkspace(workspace, tmpFile)
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function renderWorkspace(workspace, file) {
    loading();
    let customisationVals = loadCustomisation();
    WorkspaceController.render(workspace.id, file, customisationVals)
        .then(blob => {
            let filename = workspace.name.replace(/[^a-zA-Z0-9_-]/g, '_') + '.stl';
            const link = document.createElement("a");
            link.href = URL.createObjectURL(blob);
            link.download = filename;
            stopLoading();
            getLastLogWorkspace(workspace);
            link.click();
        })
        .catch(data => {
            console.error(data);
            getLastLogWorkspace(workspace);
            stopLoading();
        });
}

function createDirectory(workspace, dirPath) {
    let dirName = prompt("New directory (in path dirPath: " + dirPath + ")");
    if (!dirName) return;
    loading();
    WorkspaceFileController.createDirectory(workspace.id, dirPath + "/" + dirName)
        .then(data => {
            fetchFilesList(workspace);
            stopLoading();
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function displayWorkspace(workspace) {
    console.log(workspace);
    document.getElementById('workspaceList').style.display = 'none';
    document.getElementById('workspace').style.display = '';
    document.getElementById('workspace_id').value = workspace.id;
    document.getElementById('workspace_idDiv').innerHTML = 'ID: #' + workspace.id;
    document.getElementById('workspace_name').value = workspace.name;
    document.getElementById('workspace_previewWidth').value = workspace.previewWidth;
    document.getElementById('workspace_previewHeight').value = workspace.previewHeight;
    document.getElementById('previewDiv').style.width = workspace.previewWidth + 'px';
    document.getElementById('previewDiv').style.height = workspace.previewHeight + 'px';
}

function displayListWorkspace(workspaceList) {
    document.getElementById('workspaceList').style.display = 'block';
    document.getElementById('workspace').style.display = 'none';
    let html = "";
    for (const workspace of workspaceList) {
        html += '<li><a href="?workspaceId=' + workspace.id + '">' + workspace.name + '</a></li>workspace';
    }
    document.getElementById('workspaceListing').innerHTML = html;
}

function loadWorkspace(workspace) {
    workspace.id = document.getElementById('workspace_id').value;
    workspace.name = document.getElementById('workspace_name').value;
    workspace.previewWidth = document.getElementById('workspace_previewWidth').value;
    workspace.previewHeight = document.getElementById('workspace_previewHeight').value;
    return workspace;
}

function getValuesWorkspace(workspaceId, filePath) {
    document.getElementById('customisation').innerHTML = "";
    document.getElementById('actionBtn').innerHTML = "";
    document.getElementById('previewImage').src = "img/preview.png";
    if (!IsOpenScadFile(filePath)) {
        return;
    }
    loading();
    WorkspaceFileController.getCustomisation(workspaceId, filePath)
        .then(values => {
            document.getElementById('customisation').innerHTML = displayCustomisation(values, filePath);
            if (IsOpenScadFile(filePath)) {
                document.getElementById('actionBtn').innerHTML = ""
                    + '<button onclick="previewWorkspace(currentWorkspace, \'' + filePath + '\');" title="preview" />👁</button> '
                    + '<button onclick="renderWorkspace(currentWorkspace, \'' + filePath + '\');" title="render" /> 🔩</button> ';
            }
            loading();
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function fetchFilesList(workspace) {
    loading();
    WorkspaceFileController.listFiles(workspace.id, '')
        .then(data => {
            stopLoading();
            displayFiles(data);
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function fetchFileContent(workspace, filePath) {
    loading();
    WorkspaceFileController.getFile(workspace.id, filePath)
        .then(data => {
            data.text()
                .then(value => {
                    document.getElementById('fileContent').disabled = false;
                    document.getElementById('fileContent').readonly = false;
                    document.getElementById('fileContent').value = value;
                    stopLoading();
                });
        })
        .catch((error) => {
            console.error('Error:', error);
            stopLoading();
        });
}

function fetchImageContent(workspace, filePath) {
    let p = WorkspaceFileController.getFile(workspace.id, filePath);
    p.then(response => response.blob())
        .then(blob => {
            document.getElementById('previewImage').src = URL.createObjectURL(blob);
            getLastLogWorkspace(workspace);
            stopLoading();
        })
        .catch(data => {
            console.error(data);
            document.getElementById('previewImage').src = "img/preview_failed.png";
            getLastLogWorkspace(workspace);
            stopLoading();
        });
    p.catch(data => {
        console.error(data);
        document.getElementById('previewImage').src = "img/preview_failed.png";
        getLastLogWorkspace(workspace);
        stopLoading();
    });
}

function loadCustomisation() {
    let vals = []
    for (let customVal of document.getElementsByClassName("customisation_val")) {
        vals.push({
            "variable": customVal.id.replace("customisation__", ""),
            "value": "" + html.getValue(customVal.id)
        });
    }
    return vals;
}

function displayFiles(files) {
    let h = "";
    h += displayFilesRec(files, "");
    document.getElementById('filesList').innerHTML = h;
}

function displayFilesRec(files, path) {
    let h = "";
    for (const file of files) {
        let filePath = path + file.name;
        if (file.type === "directory") {
            h += '<div class="directory">' +
                '<div class="directory_name">📁 ' + file.name + ' ' +
                '<button onclick="createDirectory(currentWorkspace, \'' + filePath + '\');" title="add directory">+</button>' +
                '</div>' +
                '<div class="directory_content">' + displayFilesRec(file.contents, filePath + '/') + '</div>' +
                '</div>'
        } else {
            h += '<div class="file">' + '<div class="file_name">' + fileDisplay(file, filePath) + '</div>' + '</div>';
        }
    }
    return h;
}


function fileDisplay(file, filePath) {
    return '<label><input type="radio" name="fileName" value="' + filePath + '" onclick="loadFile(currentWorkspace, \'' + filePath + '\');" title="edit" />' + file.name + '</label>';
}

function displayCustomisation(values, filePath) {
    let html = "";
    for (const value of values) {
        const id = 'customisation__' + value.name;
        html += '<tr><td><label for="' + id + '">' + (value.description ? (value.description + ' (' + value.name + ')') : value.name) + '</label></td><td>' + inputCustomisation(value) + '</tr>';
    }
    return html;
}

function inputCustomisation(value) {
    const id = 'customisation__' + value.name;
    if (value.enumValues?.length) {
        return inputCustomisationEnum(id, value);
    } else if (value.type === "Boolean") {
        const checked = (value.defaultValue === 'true') ? 'checked="checked"' : '';
        return '<input type="checkbox" id="' + id + '" value="true" ' + checked + ' class="customisation_val" />';
    } else if (value.type === 'Number') {
        return inputCustomisationNumber(id, value);
    } else if (value.type === 'Vector') {
        return inputCustomisationVector(id, value);
    }
    return inputCustomisationString(id, value)
}

function inputCustomisationEnum(id, value) {
    if (isColorVariable(value)) {
        let options = "";
        for (const enumValue of value.enumValues) {
            const selected = (enumValue.value === value.defaultValue) ? 'selected="selected"' : '';
            options += '<option value="' + enumValue.value + '" ' + selected + '>' + (enumValue.description ?? enumValue.value) + '</option>';
        }
        return '<select id="' + id + '" class="customisation_val"' +
            ' onchange="document.getElementById(this.id + \'-color\').value = color2name(this.value)">' + options + '</select>' +
            '<input type="color" id="' + id + '-color" value="' + color2name(value.defaultValue) + '" ' +
            ' onchange="document.getElementById(this.id.replace(\'-color\', \'\')).value = this.value" />';
    } else {
        let options = "";
        for (const enumValue of value.enumValues) {
            const selected = (enumValue.value === value.defaultValue) ? 'selected="selected"' : '';
            options += '<option value="' + enumValue.value + '" ' + selected + '>' + (enumValue.description ?? enumValue.value) + '</option>';
        }
        return '<select id="' + id + '" class="customisation_val">' + options + '</select>';
    }
}

function inputCustomisationNumber(id, value) {
    const min = value.min || value.min === 0 ? (' min="' + value.min + '"') : '';
    const step = value.step || value.step === 0 ? (' step="' + value.step + '"') : '';
    const max = value.max || value.max === 0 ? (' max="' + value.max + '"') : '';
    return '<input type="range" id="' + id + '-range" value="' + value.defaultValue + '" ' + min + step + max + ' ' +
        ' onchange="document.getElementById(this.id.replace(\'-range\', \'\')).value = this.value" />' +
        '<input type="number" id="' + id + '" value="' + value.defaultValue + '" ' + min + step + max + ' class="customisation_val"' +
        ' onchange="document.getElementById(this.id + \'-range\').value = this.value"  />';
}

function inputCustomisationString(id, value) {
    if (isColorVariable(value)) {
        return '<input type="text" id="' + id + '" value="' + value.defaultValue + '" ' + ' class="customisation_val colorText"' +
            ' onchange="document.getElementById(this.id + \'-color\').value = color2name(this.value)"  />' +
            '<input type="color" id="' + id + '-color" value="' + color2name(value.defaultValue) + '" ' +
            ' onchange="document.getElementById(this.id.replace(\'-color\', \'\')).value = this.value" />';
    } else {
        const max = value.max ? ('maxlength="' + value.max + '"') : '';
        return '<input type="text" id="' + id + '" value="' + value.defaultValue + '" ' + max + ' class="customisation_val" />';
    }
}

function inputCustomisationVector(id, value) {
    const max = value.max ? ('maxlength="' + value.max + '"') : '';
    return '<input type="text" id="' + id + '" value="' + value.defaultValue + '" ' + max + ' class="customisation_val" />';
}

function isColorVariable(value) {
    return value.name.includes('color') || value.name.includes('color')
}

function color2name(color) {
    if (colorNameList.hasOwnProperty(color)) {
        return colorNameList[color];
    } else {
        return color;
    }
}

function IsOpenScadFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".scad")
        || lowFilePath.endsWith(".csg");
}

function IsImageFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".apng")
        || lowFilePath.endsWith(".avif")
        || lowFilePath.endsWith(".gif")
        || lowFilePath.endsWith(".jpg")
        || lowFilePath.endsWith(".jpeg")
        || lowFilePath.endsWith(".jfif")
        || lowFilePath.endsWith(".pjpeg")
        || lowFilePath.endsWith(".pjp")
        || lowFilePath.endsWith(".png")
        || lowFilePath.endsWith(".webp")
        || lowFilePath.endsWith(".bmp")
        || lowFilePath.endsWith(".ico")
        || lowFilePath.endsWith(".cur")
        || lowFilePath.endsWith(".tif")
        || lowFilePath.endsWith(".tiff");
}

function Is3dFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".stl")
        || lowFilePath.endsWith(".off")
        || lowFilePath.endsWith(".amf")
        || lowFilePath.endsWith(".3mf");
}

function IsVector3dFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".dxf");
}

function IsVectorImageFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".svg");
}

function Is2dFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".png")
        || lowFilePath.endsWith(".dat")
        || lowFilePath.endsWith(".txt")
        || lowFilePath.endsWith(".csv")
        || lowFilePath.endsWith(".tsv");
}

function IsTextFile(filePath) {
    let lowFilePath = filePath.toLowerCase();
    return lowFilePath.endsWith(".txt")
        || lowFilePath.endsWith(".md")
        || lowFilePath.endsWith(".csv")
        || lowFilePath.endsWith(".tsv");
}
