WorkspaceFileController = {
    createDirectory: function (workspaceId, directoryName) {
        let directoryPath = encodeURIComponent(directoryName);
        return fetch(`/WorkspaceFile/${workspaceId}/directory/${directoryPath}`, {
            method: 'PUT'
        });
    },
    deleteDirectory: function (workspaceId, directoryName) {
        let directoryPath = encodeURIComponent(directoryName);
        return fetch(`/WorkspaceFile/${workspaceId}/directory/${directoryPath}`, {
            method: 'DELETE'
        });
    },
    listFiles: function (workspaceId, directoryName) {
        let directoryPath = encodeURIComponent(directoryName);
        return fetch(`/WorkspaceFile/${workspaceId}/directory/${directoryPath}`)
            .then(response => response.json());
    },
    addFile: function (workspaceId, fileName, content) {
        return fetch(`/WorkspaceFile/${workspaceId}/file/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "fileName": fileName,
                "content": content
            }),
        });
    },
    uploadFile: function (workspaceId, data) {
        return fetch(`/WorkspaceFile/${workspaceId}/file/`, {
            method: 'PUT',
            body: data,
        });
    },
    getFile: function (workspaceId, filePath) {
        let path = encodeURIComponent(filePath);
        return fetch(`/WorkspaceFile/${workspaceId}/file/${path}`);
    },
    deleteFile: function (workspaceId, filePath) {
        let path = encodeURIComponent(filePath);
        return fetch(`/WorkspaceFile/${workspaceId}/file/${path}`, {
            method: 'DELETE'
        });
    },
    getCustomisation: function (workspaceId, filePath) {
        let path = encodeURIComponent(filePath);
        return fetch(`/WorkspaceFile/${workspaceId}/customisation/${path}`)
            .then(response => response.json());
    }
};
