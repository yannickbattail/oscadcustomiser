using System.Text.RegularExpressions;
using OscadCustomisationParser.Model;

namespace OscadCustomisationParser;

public class OpenscadParser
{
    public IEnumerable<Customization> Parse(string openscadCode)
    {
        return ParseBase(openscadCode).Select(MapCustomisation);
    }

    private static Customization MapCustomisation(BaseMatch c)
    {
        var constraint = ParseConstraint(c.Constraints);
        var type = VarType.Number;
        if (c.DefaultValue.Equals("true") || c.DefaultValue.Equals("false"))
        {
            type = VarType.Boolean;
        }
        else if (c.DefaultValue.StartsWith("\""))
        {
            c.DefaultValue = c.DefaultValue.Substring(1, c.DefaultValue.Length - 2);
            type = VarType.String;
        }
        else if (c.DefaultValue.StartsWith("["))
        {
            type = VarType.Vector;
        }

        return new Customization(c.VarName.Trim(), type, c.DefaultValue)
        {
            Description = c.Description.Trim(),
            Min = constraint.Min.ParseDouble(null),
            Step = constraint.Step.ParseDouble(null),
            Max = constraint.Max.ParseDouble(null),
            EnumValues = constraint.EnumValues
        };
    }

    public static Constraint ParseConstraint(string constraintStr)
    {
        var match = Regex.Match(constraintStr, "^(?<max>[0-9]*)$");
        if (match.Success) // string max length
        {
            return new Constraint
            {
                Max = match.Groups["max"].Value
            };
        }

        // remove []
        constraintStr = constraintStr.Substring(1, constraintStr.Length - 2);
        if (constraintStr.Contains(',')) // enum
        {
            return new Constraint
            {
                EnumValues = constraintStr.Split(",").Select(enumElement => new EnumValues(enumElement))
            };
        }

        var boundElements = constraintStr.Split(":");
        return boundElements.Length switch
        {
            1 => new Constraint { Max = boundElements[0] },
            2 => new Constraint { Min = boundElements[0], Max = boundElements[1] },
            3 => new Constraint { Min = boundElements[0], Step = boundElements[1], Max = boundElements[2] },
            _ => new Constraint()
        };
    }

    private static IEnumerable<BaseMatch> ParseBase(string code)
    {
        var regexVariable = new Regex(@"(\/\/(?<description>[^\n]*))?(^|\n)\s*(?<varName>\$?[a-zA-Z0-9_]*)\s*=\s*(?<defaultValue>-?([0-9Ee\.]*)|(true)|(false)|(""[^""]*"")|(\[[^\]]*\]));(( |\t)*//( |\t)*(?<constraints>.*))?");
        var regexModule = new Regex(@"(\n\s*module\s)");
        var parts = new List<BaseMatch>();
        var start = 0;
        while (true)
        {
            var match = regexVariable.Match(code, start);
            if (!match.Success)
            {
                return parts;
            }
            //var matchedStr = code[match.Index..(match.Index + match.Length)];
            if (regexModule.Match(code, start, match.Index + match.Length - start).Success)
            {
                return parts;
            }

            var baseMatch = new BaseMatch
            {
                Description = match.Groups["description"].Value.Trim(),
                VarName = match.Groups["varName"].Value.Trim(),
                DefaultValue = match.Groups["defaultValue"].Value.Trim(),
                Constraints = match.Groups["constraints"].Value.Trim()
            };
            parts.Add(baseMatch);
            start = match.Index + match.Length;
        }
    }

    private sealed class BaseMatch
    {
        public string Description { get; set; } = "";
        public string VarName { get; set; } = "";
        public string DefaultValue { get; set; } = "";
        public string Constraints { get; set; } = "";
    }

    public class Constraint
    {
        public string? Min { get; set; }
        public string? Step { get; set; }
        public string? Max { get; set; }
        public IEnumerable<EnumValues> EnumValues { get; set; } = new List<EnumValues>();
    }
}