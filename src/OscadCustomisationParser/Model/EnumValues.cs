namespace OscadCustomisationParser.Model;

public class EnumValues
{
    public EnumValues(string valueDescription)
    {
        var split = valueDescription.Split(":");
        Value = split[0].Trim();
        if (split.Length > 1)
        {
            Description = split[1].Trim();
        }
    }

    public EnumValues(string value, string description)
    {
        Value = value;
        Description = description;
    }

    public string? Description { get; set; }
    public string Value { get; set; }
}