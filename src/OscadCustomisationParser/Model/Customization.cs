﻿namespace OscadCustomisationParser.Model;

public class Customization
{
    public Customization(string name, VarType type, string defaultValue)
    {
        Name = name;
        Type = type;
        DefaultValue = defaultValue;
    }

    public string Name { get; set; }
    public string? Description { get; set; }
    public VarType Type { get; set; }
    public string DefaultValue { get; set; }
    public double? Min { get; set; }
    public double? Step { get; set; }
    public double? Max { get; set; }
    public IEnumerable<EnumValues> EnumValues { get; set; } = new List<EnumValues>();
}