using System.Text.Json.Serialization;

namespace OscadCustomisationParser.Model;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum VarType
{
    Boolean,
    Number,
    String,
    Vector
}