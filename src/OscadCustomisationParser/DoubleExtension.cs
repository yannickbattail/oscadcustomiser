namespace OscadCustomisationParser;

public static class DoubleExtension
{
    public static double? ParseDouble(this string? source, int? defaultValue)
    {
        if (source != null && double.TryParse(source, out var result))
        {
            return result;
        }

        return defaultValue;
    }
}