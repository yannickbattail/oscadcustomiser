﻿using System.Text.Json;
using OscadContainerControl;
using OscadCustomisationParser;

namespace Testing;

public static class Program
{
    public static int Main(string[] argv)
    {
        Workspace();
        //Parse();
        //ParseConstraint();
        return 0;
    }

    private static void Workspace()
    {
        var w = new ContainerWorkspace(Guid.NewGuid().ToString());
        w.Start();
        w.AddFile("pokeball.scad", "pokeball.scad");
        Console.WriteLine("Hello, World!");
        Console.WriteLine(JsonSerializer.Serialize(w.ListFiles()));
        var variables = new Dictionary<string, string>
        {
            { "top_color", "blue" }
        };
        Console.WriteLine(w.Preview(variables, "pokeball.scad"));
        Console.WriteLine(w.Render(new Dictionary<string, string>(), "pokeball.scad"));
    }

    private static void ParseConstraint()
    {
        var constraints = new List<string>
        {
            "666",
            "[20]",
            "[10:20]",
            "[10:0.5:20]",
            "[S:Small, M:Medium, L:Large, other]"
        };
        foreach (var constraint in constraints)
        {
            var c = OpenscadParser.ParseConstraint(constraint);
            Console.WriteLine("#################################");
            Console.WriteLine("Min: " + c.Min);
            Console.WriteLine("Step: " + c.Step);
            Console.WriteLine("Max: " + c.Max);
            Console.WriteLine("EnumValues: " +
                              string.Join(",", c.EnumValues.Select(ev => ev.Value + ":" + ev.Description)));
        }
    }

    private static void Parse()
    {
        var code =
            "//labeled combo box for string\nLabeled_value=\"S\"; // [S:Small, M:Medium, L:Large]\n//some description\nvariable1=12.5; // [10:0.5:20]\nbooleanFlag=true;\na_string_with_max=\"plop\";//4";
        foreach (var c in new OpenscadParser().Parse(code))
        {
            Console.WriteLine("#################################");
            Console.WriteLine("Name: " + c.Name);
            Console.WriteLine("Description: " + c.Description);
            Console.WriteLine("DefaultValue: " + c.DefaultValue);
            Console.WriteLine("Type: " + c.Type);
            Console.WriteLine("Min: " + c.Min);
            Console.WriteLine("Step: " + c.Step);
            Console.WriteLine("Max: " + c.Max);
            Console.WriteLine("EnumValues: " +
                              string.Join(",", c.EnumValues.Select(ev => ev.Value + ":" + ev.Description)));
        }
    }
}