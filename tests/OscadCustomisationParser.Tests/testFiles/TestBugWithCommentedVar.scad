// rounding of the dice
rounding = 0.69; // [0.6:0.01:0.8]
// hole size/depth
hole_size = 0.56; // [0.5:0.01:0.591]
// dice color
dice_color = "yellow"; // [yellow, red, green, blue, white, black]
// holes color
holes_color = "red"; // [yellow, red, green, blue, white, black]

/* [Animation] */
// Animation rotation
animation_rotation = false;

/* [Hidden] */
$vpt = animation_rotation?[0, 0, 0]:$vpt;
$vpr = animation_rotation?[60, 0, 365 * $t]:$vpr; // animation rotate around the object
$vpd = animation_rotation?200:$vpd;

// $fn=100;
scale(50) dice(rounding, hole_size, dice_color, holes_color);
module dice(rounding, face, dice_color, holes_color) {
    c = 0.22;
}
