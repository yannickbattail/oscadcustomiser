using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using OscadCustomisationParser.Model;
using Xunit;

namespace OscadCustomisationParser.Tests;

public class OscadCustomisationParserTests
{
    public static IEnumerable<object[]> TestData =>
        new List<object[]>
        {
            new object[]
            {
                "var_name=1;", new List<Customization>
                {
                    new("var_name", VarType.Number, "1")
                    {
                        Description = ""
                    }
                }
            },
            new object[]
            {
                "//labeled combo box for string\nLabeled_value=\"S\"; // [S:Small, M:Medium, L:Large]\n",
                new List<Customization>
                {
                    new("Labeled_value", VarType.String, "S")
                    {
                        Description = "labeled combo box for string",
                        EnumValues = new List<EnumValues>
                        {
                            new("S", "Small"),
                            new("M", "Medium"),
                            new("L", "Large")
                        }
                    }
                }
            },
            new object[]
            {
                "variable1=12.5; // [20]\n", new List<Customization>
                {
                    new("variable1", VarType.Number, "12.5")
                    {
                        Description = "",
                        Max = 20
                    }
                }
            },
            new object[]
            {
                "variable2=12.5; // [10:20]\n", new List<Customization>
                {
                    new("variable2", VarType.Number, "12.5")
                    {
                        Description = "",
                        Min = 10,
                        Max = 20
                    }
                }
            },
            new object[]
            {
                "variable3=12.5; // [10:0.5:20]\n", new List<Customization>
                {
                    new("variable3", VarType.Number, "12.5")
                    {
                        Description = "",
                        Min = 10,
                        Step = 0.5,
                        Max = 20
                    }
                }
            },
            new object[]
            {
                "booleanFlag=true;\n", new List<Customization>
                {
                    new("booleanFlag", VarType.Boolean, "true")
                    {
                        Description = ""
                    }
                }
            },
            new object[]
            {
                "a_string_with_max=\"plop\";// 4\n", new List<Customization>
                {
                    new("a_string_with_max", VarType.String, "plop")
                    {
                        Description = "",
                        Max = 4
                    }
                }
            },
            new object[]
            {
                "vect=[0.5,3.6,\"lol\"];\n", new List<Customization>
                {
                    new("vect", VarType.Vector, "[0.5,3.6,\"lol\"]")
                    {
                        Description = ""
                    }
                }
            },
            new object[]
            {
                "// int A\nintegerA=1;// [0:9]\n// int B\nintegerB=1; // [0:9] \n", new List<Customization>
                {
                    new("integerA", VarType.Number, "1")
                    {
                        Description = "int A",
                        Min = 0,
                        Max = 9
                    },
                    new("integerB", VarType.Number, "1")
                    {
                        Name = "integerB",
                        Type = VarType.Number,
                        DefaultValue = "1",
                        Description = "int B",
                        Min = 0,
                        Max = 9
                    }
                }
            },
            new object[]
            {
                "$fn=100;", new List<Customization>
                {
                    new("$fn", VarType.Number, "100")
                    {
                        Description = ""
                    }
                }
            }
        };


    public static IEnumerable<object[]> TestDataEnd =>
        new List<object[]>
        {
            new object[]
            {
                "var1=1;\nvar2=1;\nfunction f(x)=x^2;\nvar3=1;", new List<Customization>
                {
                    new("var1", VarType.Number, "1") { Description = "" },
                    new("var2", VarType.Number, "1") { Description = "" },
                    new("var3", VarType.Number, "1") { Description = "" }
                }
            },
            new object[]
            {
                "var1=1;\nvar2=1;\nmodule f(x) {cube(10);}\nvar3=1;",
                new List<Customization>
                {
                    new("var1", VarType.Number, "1") { Description = "" },
                    new("var2", VarType.Number, "1") { Description = "" }
                }
            }
        };

    [Theory]
    [MemberData(nameof(TestData))]
    public void TestAllVariables(string code, IEnumerable<Customization> expected)
    {
        new OpenscadParser().Parse(code).Should().BeEquivalentTo(expected);
    }


    [Theory]
    [MemberData(nameof(TestDataEnd))]
    public void TestEnd(string code, IEnumerable<Customization> expected)
    {
        new OpenscadParser().Parse(code).Should().BeEquivalentTo(expected);
    }


    [Fact]
    public void TestBugWithVarDollarFn()
    {
        var code = File.ReadAllText("testFiles/TestBugWithVarDollarFn.scad");
        IEnumerable<Customization> expected = new List<Customization>
        {
            new("rounding", VarType.Number, "0.69")
            {
                Description = "rounding of the dice",
                Min = 0.6,
                Step = 0.01,
                Max = 0.8
            },
            new("hole_size", VarType.Number, "0.56")
            {
                Description = "hole size/depth",
                Min = 0.5,
                Step = 0.01,
                Max = 0.591
            },
            new("dice_color", VarType.String, "yellow")
            {
                Description = "dice color",
                EnumValues = new List<EnumValues>
                {
                    new("yellow"),
                    new("red"),
                    new("green"),
                    new("blue"),
                    new("white"),
                    new("black")
                }
            },
            new("holes_color", VarType.String, "red")
            {
                Description = "holes color",
                EnumValues = new List<EnumValues>
                {
                    new("yellow"),
                    new("red"),
                    new("green"),
                    new("blue"),
                    new("white"),
                    new("black")
                }
            },
            new("animation_rotation", VarType.Boolean, "false")
            {
                Description = "Animation rotation"
            },
            new("$fn", VarType.Number, "40") { Description = "" }
        };
        new OpenscadParser().Parse(code).Should().BeEquivalentTo(expected);
    }

    [Fact]
    public void TestBugWithCommentedVar()
    {
        var code = File.ReadAllText("testFiles/TestBugWithCommentedVar.scad");
        IEnumerable<Customization> expected = new List<Customization>
        {
            new("rounding", VarType.Number, "0.69")
            {
                Description = "rounding of the dice",
                Min = 0.6,
                Step = 0.01,
                Max = 0.8
            },
            new("hole_size", VarType.Number, "0.56")
            {
                Description = "hole size/depth",
                Min = 0.5,
                Step = 0.01,
                Max = 0.591
            },
            new("dice_color", VarType.String, "yellow")
            {
                Description = "dice color",
                EnumValues = new List<EnumValues>
                {
                    new("yellow"),
                    new("red"),
                    new("green"),
                    new("blue"),
                    new("white"),
                    new("black")
                }
            },
            new("holes_color", VarType.String, "red")
            {
                Description = "holes color",
                EnumValues = new List<EnumValues>
                {
                    new("yellow"),
                    new("red"),
                    new("green"),
                    new("blue"),
                    new("white"),
                    new("black")
                }
            },
            new("animation_rotation", VarType.Boolean, "false")
            {
                Description = "Animation rotation"
            }
        };
        new OpenscadParser().Parse(code).Should().BeEquivalentTo(expected);
    }

    [Fact]
    public void TestBugWithNoPreviousRange()
    {
        var code = File.ReadAllText("testFiles/TestBugWithNoPreviousRange.scad");
        IEnumerable<Customization > expected = new List<Customization>
        {
            new("$fn", VarType.Number, "1")
            {
                Description = "number of fragments",
                Min = 10,
                Step = 1,
                Max = 200
            },
            new("$fa", VarType.Number, "1")
            {
                Description = "minimum angle",
            },
            new("$fs", VarType.Number, "1")
            {
                Description = "minimum size",
            }
        };
        new OpenscadParser().Parse(code).Should().BeEquivalentTo(expected);
    }
}
