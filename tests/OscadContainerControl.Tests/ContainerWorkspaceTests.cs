using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Moq;
using Xunit;

namespace OscadContainerControl.Tests;

public class ContainerWorkspaceTests
{
    [Fact]
    public void TestCreateDirectory()
    {
        var cw = new ContainerWorkspace("ID");

        var dockerContainer = new Mock<DockerContainer>();
        dockerContainer.Setup(x => x.Exec(It.IsAny<string>())).Returns("");
        cw.Container = dockerContainer.Object;
        cw.CreateDirectory("platypus");

        dockerContainer.Verify(x => x.Exec("mkdir -p \"/workspace/platypus\""));
    }

    [Fact]
    public void TestCreateDirectoryWithQuotes()
    {
        var cw = new ContainerWorkspace("ID");

        var dockerContainer = new Mock<DockerContainer>();
        dockerContainer.Setup(x => x.Exec(It.IsAny<string>())).Returns("");
        cw.Container = dockerContainer.Object;
        cw.CreateDirectory("platypus \"panda's");

        dockerContainer.Verify(x => x.Exec("mkdir -p \"/workspace/platypus \\\"panda's\""));
    }
    [Fact]
    public void TestCreateDirectoryWith2Points()
    {
        var cw = new ContainerWorkspace("ID");

        var dockerContainer = new Mock<DockerContainer>();
        dockerContainer.Setup(x => x.Exec(It.IsAny<string>())).Returns("");
        cw.Container = dockerContainer.Object;
        cw.CreateDirectory("../platypus/..../cat");

        dockerContainer.Verify(x => x.Exec("mkdir -p \"/workspace/./platypus/./cat\""));
    }
    [Fact]
    public void TestBuildVariablesNumber()
    {
        var variable = new Dictionary<string, string>()
        {
            {"var", "42"}
        };
        var actual = ContainerWorkspace.BuildVariables(variable);

        actual.Should().Be(" -D \"var=42\" ");
    }
    [Fact]
    public void TestBuildVariablesString()
    {
        var variable = new Dictionary<string, string>()
        {
            {"var", "panda"}
        };
        var actual = ContainerWorkspace.BuildVariables(variable);

        actual.Should().Be(" -D \"var=\\\"panda\\\"\" ");
    }
    [Fact]
    public void TestBuildVariablesStringSpecialChar()
    {
        var variable = new Dictionary<string, string>()
        {
            {"var", "panda's /!\\  \"b&w\" "}
        };
        var actual = ContainerWorkspace.BuildVariables(variable);

        actual.Should().Be(" -D \"var=\\\"panda's /!\\\\  \\\"b&w\\\" \\\"\" ");
    }
    [Fact]
    public void TestListFiles()
    {
        var cw = new ContainerWorkspace("ID");

        var dockerContainer = new Mock<DockerContainer>();
        dockerContainer.Setup(x => x.Exec(It.IsAny<string>())).Returns("[{\"type\":\"directory\",\"name\":\".\",\"contents\":[]},{\"type\":\"report\",\"directories\":0,\"files\":0}]");
        cw.Container = dockerContainer.Object;
        var actual = cw.ListFiles("platypus");

        dockerContainer.Verify(x => x.Exec("tree -a -J \"platypus\""));
        var expected = new List<FileDescription>()
        {
            new()
            {
                type = "directory",
                name = ".",
                contents = Enumerable.Empty<FileDescription>()
            }
        };
        actual.Should().BeEquivalentTo(expected);
    }
}
